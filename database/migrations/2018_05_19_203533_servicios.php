<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->string('dias');
            $table->time('hora_desde');
            $table->time('hora_hasta');
            $table->date('fecha_inic');
            $table->date('fecha_final');
            $table->integer('identificacion')->length(12)->unsigned();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('empresas');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
