<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Resoluciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resoluciones', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->string('resolucion');
            $table->string('prefijo')->null();
            $table->string('numero_inic');
            $table->string('numero_final');
            $table->string('preferida');
            $table->enum('estado',['Activo','Desactivado'])->default('Activo');
            $table->integer('identificacion')->length(12)->unsigned();
            $table->timestamps();
            $table->foreign('identificacion')->references('identificacion')->on('empresas');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
