<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ordenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->integer('id_plan')->length(12)->unsigned();
            $table->integer('identificacion')->length(12)->unsigned();
            $table->date('fecha_inic');
            $table->date('fecha_venc');
            $table->enum('estado',['Pagado','No pagado']);
            $table->enum('estado_servicio',['activo','suspendido']);
            $table->timestamps();
            $table->foreign('id_plan')->references('id')->on('planes');
            $table->foreign('identificacion')->references('identificacion')->on('empresas');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
