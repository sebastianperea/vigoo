<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('facturas', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('identificacion')->unsigned();
            $table->date('fecha_factura');
            $table->date('fecha_venc');
            $table->string('notas');
            $table->string('subtotal');
            $table->string('iva');
            $table->string('total');
            $table->foreign('identificacion')->references('identificacion')->on('empresas');
            $table->timestamps();

            }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
