<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            
            
            $table->primary('identificacion')->unsigned();
            $table->integer('identificacion')->unsigned();
            $table->string('tipodepersona');
            $table->enum('tipo_de_identificacion',['NIT','CC','PAS','CE'])->default('NIT');
            $table->integer('dv');
            $table->string('razon_social');
            $table->string('departamento');
            $table->string('ciudad');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('emailfac');
            $table->string('logo');
            $table->string('sitioweb');
            $table->enum('estado',['Activo','Inactivo'])->default('Activo');
            $table->timestamps();


            }); //
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }

}