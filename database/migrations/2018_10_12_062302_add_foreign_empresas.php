<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('fecha_nac')->after('password');
            $table->string('genero')->after('password');
            $table->string('peso')->after('password');
            $table->string('estatura')->after('password');
            $table->string('diaentreno')->after('password');
            $table->dropColumn('type');
        });

        Schema::table('users', function (Blueprint $table) {
            
            $table->enum('type',['admin','gymadmin','cliente'])->default('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
