<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacturasPlanes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
     
 Schema::create('facturas_planes', function (Blueprint $table){

            $table->increments('id');
            $table->integer('facturas_id')->unsigned();
            $table->integer('planes_id')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->integer('precio')->unsigned();
            

            

            $table->foreign('facturas_id')->references('id')->on('facturas')->onDelete('cascade');
            $table->foreign('planes_id')->references('id')->on('planes'); 
               

            

         

        }); 

}


    
    public function down()
    {
      
    }

}