@extends('template.admin.main')
@section('titulo', 'Planes')


@section('content')




<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            {!! Form::open(['route' => ['planes.update',$planes],'method' => 'PUT']) !!} 
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información básica de la empresa</p>
                                <div class="row p-t-20">
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('nombre','NOMBRE DEL PLAN')!!}
                                            {!!Form::text('nombre',$planes->nombre, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombre' , 'required']) !!}
                                         </div>
                                    </div> 
                                    
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('precio','PRECIO')!!}
                                            {!!Form::number('precio',$planes->precio, ['class' => 'form-control input-rounded', 'id'=> 'precio', 'placeholder' => 'Precio' , 'required', 'min'=>'0']) !!}
                                         </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('detalles','DETALLES')!!}
                                            {!!Form::textarea('detalles',$planes->detalles, ['class' => 'form-control input-rounded', 'id'=> 'detalles', 'placeholder' => 'Detalles' , 'required']) !!}
                                         </div>

                                    </div>

                                </div> 
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection            