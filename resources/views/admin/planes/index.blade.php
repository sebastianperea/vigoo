@extends('template.admin.main')

@section('titulo', 'Planes')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Planes</h4>
                                <h6 class="card-subtitle">Todos los planes</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>PRECIO</th>
                                                <th>FECHA DE CREACIÓN</th>
                                                <th>ACCIONES</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($planes as $plan)
                                            <tr>
                                                <td>{{$plan->id}}</td>
                                                <td>{{$plan->nombre}}</td>
                                                <td>{{$plan->precio}}</td>
                                                <td>{{$plan->created_at}}</td>
                                                <td><a href="{{ route('planes.edit', $plan->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('admin.planes.destroy' , $plan->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                            </tr>  
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
