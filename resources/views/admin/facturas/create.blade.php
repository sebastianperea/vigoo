@extends('template.admin.main')
@section('titulo', 'Facturación')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 

                               
                                
                                {!! Form::open(['route' => 'facturas.store' ,'method' => 'POST']) !!}

                                        <div style="text-align:right;">
                                        <h2 class="card-title">Factura # {{$numerofactura}}</h2><br>

                                    </div>
                                

                                <div class="row p-t-20">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                         {!! Form::label('identificacion','Cliente')!!}
                                            {!!Form::select('identificacion', $empresas,null,['class' => ' js-example-basic-single form-control input-rounded','required'])!!}      
                                    </div>
                                </div>
   
                                </div>   
                                <div class="row p-t-20">
                                   
                                    <div class="col-sm-4 ">
                                                                     
                                        {!! Form::label('notas','Notas:')!!}
                                            {!!Form::textarea('notas',null, ['size' => '30x5', 'required']) !!}
                                         
                                    </div>
  
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('fecha_factura','Fecha Factura')!!}
                                            {!!Form::date('fecha_factura',null, ['class' => 'form-control input-rounded', 'required']) !!}
                                         </div>
                                    </div> 
                                    


                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('fecha_venc','Fecha vencimiento')!!}
                                            {!!Form::date('fecha_venc',null, ['class' => 'form-control input-rounded', 'required']) !!}
                                         </div>
                                    </div>
                            
                                </div> 

                                    <hr>

                                     <div class="row clearfix">
                            <div class="col-md-12">
                              <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                  <tr>
                                    <th class="text-center"> # </th>
                                    <th class="text-center"> Producto </th>
                                    <th class="text-center"> Cantidad </th>
                                    <th class="text-center"> Precio </th>
                                    <th class="text-center"> Total </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr id='addr0'>
                                    <td>1</td>
                                    <td><input type="text" name='producto_[]'  placeholder='Ingrese nombre producto' class="form-control producto" id="producto" /></td>
                                    <td><input type="number" name='cantidad_[]' placeholder='Ingrese Cantidad' class="form-control qty" step="0" min="0" id="cantidad" /></td>
                                    <td><input type="number" name='preciou_[]' placeholder='Precio Unitario' class="form-control price precio" step="0.00" min="0" id="precio" /></td>
                                    <td><input type="number" name='totalp_[]' placeholder='0.00' class="form-control total" readonly/></td>
                                  </tr>
                                  <tr id='addr1'></tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="row clearfix">
                            <div class="col-md-12">
                              <a id="add_row" class="btn btn-default pull-left">Añadir fila</a>
                              <a id='delete_row' class="pull-right btn btn-default">Borrar Fila</a>
                            </div>
                          </div>
                          <div class="row clearfix" style="margin-top:20px">
                            <div class="pull-right col-md-4">
                              <table class="table table-bordered table-hover" id="tab_logic_total">
                                <tbody>
                                  <tr>
                                    <th class="text-center">Sub Total</th>
                                    <td class="text-center"><input type="number" name='subtotal' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                                  </tr>
                                  <tr>
                                    <th class="text-center">I.V.A %</th>
                                    <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                        <input type="number" class="form-control" id="tax" placeholder="0">
                                        <div class="input-group-addon">%</div>
                                      </div></td>
                                  </tr>
                                  <tr>
                                    <th class="text-center">I.V.A</th>
                                    <td class="text-center"><input type="number" name='iva' id="tax_amount" placeholder='0.00' class="form-control" readonly/></td>
                                  </tr>
                                  <tr>
                                    <th class="text-center">Total</th>
                                    <td class="text-center"><input type="number" name='total' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
  
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>

                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection 

@section('js')

<script type="text/javascript">


    $(document).ready(function(){
    var i=1;
    $("#add_row").click(function(){b=i-1;
        $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
        $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
        i++; 
    });
    $("#delete_row").click(function(){
        if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
        }
        calc();
    });
    
    $('#tab_logic tbody').on('keyup change',function(){
        calc();
    });
    $('#tax').on('keyup change',function(){
        calc_total();
    });
    

});

function calc()
{
    $('#tab_logic tbody tr').each(function(i, element) {
        var html = $(this).html();
        if(html!='')
        {
            var qty = $(this).find('.qty').val();
            var price = $(this).find('.price').val();
            $(this).find('.total').val(qty*price);
            
            calc_total();
        }
    });
}

function calc_total()
{
    total=0;
    $('.total').each(function() {
        total += parseInt($(this).val());
    });
    $('#sub_total').val(total.toFixed(2));
    tax_sum=total/100*$('#tax').val();
    $('#tax_amount').val(tax_sum.toFixed(2));
    $('#total_amount').val((tax_sum+total).toFixed(2));
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
    <script type="text/javascript">
        function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#blah')
                                .attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

     </script>

    

@endsection     