@extends('template.admin.main')

@section('titulo', 'Facturas')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Facturas</h4>
                                <h6 class="card-subtitle">Todas las facturas</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>RAZÓN SOCIAL</th>
                                                <th>FECHA FACTURA</th>
                                                <th>FECHA DE VENCIMIENTO</th>

                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($facturas as $factura)
                                            <tr>
                                                <td>{{$factura->id}}</td>
                                              <td>{{$factura->razon_social}}</td>
                                              <td>{{$factura->fecha_factura}}</td>
                                              <td>{{$factura->fecha_venc}}</td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
