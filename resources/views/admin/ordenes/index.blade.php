@extends('template.admin.main')

@section('titulo', 'Ordenes')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ordenes</h4>
                                <h6 class="card-subtitle">Todas las ordenes</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>RAZÓN SOCIAL</th>
                                                <th>FECHA DE INICIO SERVICIO</th>
                                                <th>FECHA DE VENCIMIENTO</th>
                                                <th>ESTADO</th>
                                                <th>ESTADO DE SERVICIO</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ordenes as $orden)
                                            <tr>
                                                <td>{{$orden->id}}</td>
                                              <td>{{$orden->razon_social}}</td>
                                              <td>{{$orden->fecha_inic}}</td>
                                              <td>{{$orden->fecha_venc}}</td>
                                              <td> @if($orden->estado=="No pagado")    
                                                 <span class="label label-danger label-rounded">{{$orden->estado}}</span>
                                                 @else
                                                <span class="label label-success    label-rounded">{{$orden->estado}}</span>
                                                 @endif
                                             </td>
                                             <td> @if($orden->estado_servicio=="suspendido")    
                                                 <span class="label label-danger label-rounded">{{$orden->estado_servicio}}</span>
                                                 @else
                                                <span class="label label-success    label-rounded">{{$orden->estado_servicio}}</span>
                                                 @endif
                                             </td>
                                             <td><a href="{{ route('ordenes.show', $orden->id) }}" class="btn btn-info btn-xs" ><span class="fa fa-eye"> </a> </td>

                                                 
                                              
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
