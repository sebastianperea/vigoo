@extends('template.admin.main')
@section('titulo', 'Ordenes')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 

                                {!! Form::open(['route' => 'ordenes.store' ,'method' => 'POST']) !!}

                                <div class="row p-t-20">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                         {!! Form::label('identificacion','Cliente')!!}
                                            {!!Form::select('identificacion', $empresas,null,['class' => ' js-example-basic-single form-control input-rounded','required'])!!}      
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                        <div class="form-group">
                                         {!! Form::label('id_plan','Plan')!!}
                                            {!!Form::select('id_plan', $planes,null,['class' => ' js-example-basic-single form-control input-rounded','required'])!!}      
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('fecha_inic','Fecha Inicio de servicio')!!}
                                            {!!Form::date('fecha_inic',null, ['class' => 'form-control input-rounded', 'required']) !!}
                                         </div>
                                    </div> 
                                </div>   
                                <div class="row p-t-20">

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('fecha_venc','Fecha vencimiento')!!}
                                            {!!Form::date('fecha_venc',null, ['class' => 'form-control input-rounded', 'required']) !!}
                                         </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                         {!! Form::label('estado','Estado')!!}
                                            {!!Form::select('estado', ['Pagado'=> 'Pagado', 'No pagado' => 'No pagado'],null,['class' => 'form-control input-rounded','required'])!!}      
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                        <div class="form-group">
                                         {!! Form::label('estado_servicio','Estado Servicio')!!}
                                            {!!Form::select('estado_servicio', ['activo'=> 'activo', 'suspendido' => 'suspendido'],null,['class' => 'form-control input-rounded','required'])!!}      
                                    </div>
                                </div>
                            
                                </div> 

                                    <hr>

                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection 

@section('js')

<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>

@endsection
