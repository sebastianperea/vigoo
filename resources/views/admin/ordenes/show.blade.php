@extends('template.admin.main')
@section('titulo', 'Ordenes')


@section('content')
<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> Orden numero: {{$ordenes->id}}<br>
                            Razón social: {{$ordenes->razon_social}}<br>
                            Plan: {{$planes->nombre}}<br>
                            Detalles: {{$planes->detalles}}<br>
                            Precio: ${{$planes->precio}} </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
@endsection