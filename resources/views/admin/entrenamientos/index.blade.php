@extends('template.admin.main')

@section('titulo', 'Entrenamientos')
@section('content')



<div class="container-fluid" id="crud">
    



                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Entrenamientos</h4>

                                <h6 class="card-subtitle">Todos los entrenamientos</h6>
                                

                                <div class="box">
                                    <div>
                                        <p>
                                            <a href="#" class="btn btn-success btn pull-right" data-toggle="modal" data-target="#create">
                                                <span class="icon is-small">
                                                    <i class="fa fa-plus" style="color: white"></i>
                                                </span>
                                                <span style="color: white;">Añadir nuevo entrenamiento</span>
                                            </a>
                                        </p>
                                    </div><br>
                                
                                <div class="table-responsive m-t-40">
                                    
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>FECHA CREACIÓN</th>
                                                <th>ULTIMA ACTUALIZACIÓN</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($entrenamientos as $entrenamiento)
                                            <tr>
                                                <td>{{$entrenamiento->id}}</td>
                                              <td>{{$entrenamiento->nombre}}</td>
                                              <td>{{$entrenamiento->created_at}}</td>
                                              <td>{{$entrenamiento->updated_at}}</td>
                                              <td><a href="{{ route('manage_entrenamientos.edit', $entrenamiento->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('manage_entrenamientos.destroy' , $entrenamiento->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                        </tbody>
                                        @endforeach
                                    </table>


                                </div>
                            </div>
                                @include('admin.entrenamientos.create')
                        </div>
                        
                    </div>
                </div>

            </div> 


          
@endsection

@section('js')

<script>

new Vue({
    el: '#crud',
    created: function() {
        this.getKeeps();
    },
    data: {
        keeps: [],
        pagination: {
            'total': 0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0
        },
        newManage_entrenamientos: '',
        fillKeep: {'id': '', 'nombre': '', 'identificacion': '','default': '1' },
        errors: '',
        offset: 3,
    },
    computed: {
        isActived: function() {
            return this.pagination.current_page;
        },
        pagesNumber: function() {
            if(!this.pagination.to){
                return [];
            }

            var from = this.pagination.current_page - this.offset; 
            if(from < 1){
                from = 1;
            }

            var to = from + (this.offset * 2); 
            if(to >= this.pagination.last_page){
                to = this.pagination.last_page;
            }

            var pagesArray = [];
            while(from <= to){
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },
    methods: {
        getKeeps: function(page) {
            var urlKeeps = 'tasks?page='+page;
            axios.get(urlKeeps).then(response => {
                this.keeps = response.data.tasks.data,
                this.pagination = response.data.pagination
            });
        },
        editKeep: function(keep) {
            this.fillKeep.id   = keep.id;
            this.fillKeep.keep = keep.keep;
            $('#edit').modal('show');
        },
        updateKeep: function(id) {
            var url = 'tasks/' + id;
            axios.put(url, this.fillKeep).then(response => {
                this.getKeeps();
                this.fillKeep = {'id': '', 'keep': ''};
                this.errors   = [];
                $('#edit').modal('hide');
                toastr.success('Tarea actualizada con éxito');
            }).catch(error => {
                this.errors = 'Corrija para poder editar con éxito'
            });
        },
        deleteKeep: function(keep) {
            var url = 'tasks/' + keep.id;
            axios.delete(url).then(response => { //eliminamos
                this.getKeeps(); //listamos
                toastr.success('Eliminado correctamente'); //mensaje
            });
        },
        createKeep: function() {
            var url = 'manage_entrenamientos';
            axios.post(url, {
                nombre: this.newManage_entrenamientos
            }).then(response => {
                this.getKeeps();
                this.newManage_entrenamientos = '';
                this.errors = [];
                $('#create').modal('hide');
                toastr.success('Nueva tarea creada con éxito');
            }).catch(error => {
                this.errors = 'Corrija para poder crear con éxito'
            });
        },
        changePage: function(page) {
            this.pagination.current_page = page;
            this.getKeeps(page);
        }
    }
});
















</script>
@endsection

