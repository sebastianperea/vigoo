@extends('template.admin.main')
@section('titulo', 'Creación de empresas')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información básica de la empresa</p>
                                {!! Form::open(['route' => 'empresas.store','files' => true ,'method' => 'POST']) !!}



                                <div class="row p-t-20">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                        {!! Form::label('tipodepersona','TIPO DE PERSONA')!!}                              
                                        <input type="radio" name="tipodepersona" value="natural"> Natural<br>
                                    <input type="radio" name="tipodepersona" value="juridica"> Juridica<br>
                                         </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="logo" >Logo Empresa</label>
                                        <img id="blah" class="logoempresa" src=""/> 
                                        <input type='file' name="logo" class="logocreate" onchange="readURL(this);" class="btn btn-small"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('sitioweb','Sitio Web')!!}
                                            {!!Form::text('sitioweb',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Sitio Web' , 'required']) !!}
                                         </div>
                                    </div>

                                    
                                            

                                </div>   
                                <div class="row p-t-20">
                                    

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('tipodeidentificacion','TIPO DE IDENTIFICACIÓN')!!}
                                            {!!Form::select('tipodeidentificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],null,['class' => 'form-control input-rounded', 'id'=>'tipoid','required'])!!}
                                         </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('identificacion','IDENTIFICACIÓN')!!}
                                            {!!Form::number('identificacion',null, ['class' => 'form-control input-rounded', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required', 'min'=>'0']) !!}
                                         </div>

                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">                              
                                        {!! Form::label('dv','DV')!!}
                                            {!!Form::number('dv',null, ['class' => 'form-control input-rounded', 'id'=> 'dv', 'placeholder' => 'DV' , 'required', 'min'=>'0']) !!}
                                         </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('razon_social','RAZÓN SOCIAL')!!}
                                            {!!Form::text('razon_social',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Razón Social' , 'required']) !!}
                                         </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('departamento','DEPARTAMENTO')!!}
                                            {!!Form::text('departamento',null, ['class' => 'form-control input-rounded', 'placeholder' => 'departamento' , 'required']) !!}
                                         </div>
                                    </div> 

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('ciudad','CIUDAD')!!}
                                            {!!Form::text('ciudad',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Ciudad' , 'required']) !!}
                                         </div>
                                    </div>    

                                    <div class="col-sm-5">
                                        <div class="form-group">                              
                                        {!! Form::label('direccion','DIRECCIÓN')!!}
                                            {!!Form::text('direccion',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Dirección' , 'required']) !!}
                                         </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('telefono','TÉLEFONO')!!}
                                            {!!Form::number('telefono',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Télefono' , 'required', 'min'=>'0']) !!}
                                         </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('emailfac','EMAIL FACTURACIÓN')!!}
                                            {!!Form::email('emailfac',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Email Facturación' , 'required']) !!}
                                         </div>
                                    </div>
                                    


                                </div> 

                                    <hr>

                                    <h4 class="card-title">Información de ingreso</h4>
                                    <p class="text-muted m-b-15 f-s-12">Información necesaria para el ingreso al portal</p>

                                    <div class="row p-t-20">
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('name','NOMBRES Y APELLIDOS')!!}
                                            {!!Form::text('name',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombres y apellidos' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('email','EMAIL')!!}
                                            {!!Form::email('email',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Email' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                {!! Form::label('pass','CONTRASEÑA')!!}
                                                <div class="input-group">
                                                    <div class="input-group-addon">                              
                                                    {!!Form::password('password', ['class' => 'form-control input-rounded', 'placeholder' => 'Contraseña' , 'required', 'id'=>'pwd2']) !!}
                                                 </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                    
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection 

@section('js')


</script>
    <script type="text/javascript">
        function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#blah')
                                .attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

     </script>

@endsection     