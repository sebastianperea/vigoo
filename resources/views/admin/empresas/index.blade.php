@extends('template.admin.main')

@section('titulo', 'Empresas')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Empresas</h4>
                                <h6 class="card-subtitle">Todas las empresas</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>IDENTIFICACIÓN</th>
                                                <th>RAZÓN SOCIAL</th>
                                                <th>DIRECCIÓN</th>
                                                <th>TÉLEFONO</th>
                                                <th>ESTADO</th>
                                                <th>ACCIÓN</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($empresas as $empresa)
                                            <tr>
                                                <td>{{$empresa->identificacion}}</td>
                                                <td>{{$empresa->razon_social}}</td>
                                                <td>{{$empresa->direccion}}</td>
                                                <td>{{$empresa->telefono}}</td>
                                                <td>{{$empresa->estado}}</td>
                                                <td><a href="{{ route('empresas.edit', $empresa->identificacion) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a> </td>
                                            </tr>  
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
