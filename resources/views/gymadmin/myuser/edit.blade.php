@extends('template.gymadmin.main')
@section('titulo', 'Editar Mi usuario')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                
                                <h4 class="card-title">Mi Usuario</h4>
                                <p class="text-muted m-b-15 f-s-12">Editar Mi usuario</p>
                                {!! Form::open(['route' => 'gymadmin.myuser.update','files' => true ,'method' => 'PUT']) !!}

                                    <hr>


                                    <div class="row p-t-20">
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('name','NOMBRES Y APELLIDOS')!!}
                                            {!!Form::text('name',$users->name, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombres y apellidos' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('email','EMAIL')!!}
                                            {!!Form::email('email',$users->email, ['class' => 'form-control input-rounded', 'placeholder' => 'Email' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                {!! Form::label('pass','CONTRASEÑA')!!}
                                                <div class="input-group">
                                                    <div class="input-group-addon">                              
                                                    {!!Form::password('password', ['class' => 'form-control input-rounded', 'placeholder' => 'Contraseña' , 'id'=>'pwd2']) !!}
                                                 </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                    
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection 

@section('js')


</script>
    <script type="text/javascript">
        function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#blah')
                                .attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

     </script>

@endsection     