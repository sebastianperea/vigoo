@extends('template.gymadmin.main')
@section('titulo', 'Mi perfil')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-two">
                                    <header>
                                        <div class="avatar">
                                            <img src="" alt="Logo" />
                                        </div>
                                    </header>

                                    <h3>{{Auth::user()->name}}</h3>
                                    <div class="desc">
                                        {{$empresa->razon_social}}
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                               
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#usuario" role="tab">Mi Usuario</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#suscripcion" role="tab">Mi suscripción</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#empresa" role="tab">Empresa</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <!--First tab-->
                                <div class="tab-pane active" id="usuario" role="tabpanel">
                                    <div class="card-body">

                                <br>
                                <h4 class="card-title">Mi Usuario</h4>
                                <p class="text-muted m-b-15 f-s-12">Editar Mi usuario</p>
                                {!! Form::open(['route' => 'gymadmin.myuser.update','files' => true ,'method' => 'PUT']) !!}

                                    <hr>


                                    <div class="row p-t-20">
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('name','NOMBRES Y APELLIDOS')!!}
                                            {!!Form::text('name',$users->name, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombres y apellidos' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">                              
                                        {!! Form::label('email','EMAIL')!!}
                                            {!!Form::email('email',$users->email, ['class' => 'form-control input-rounded', 'placeholder' => 'Email' , 'required']) !!}
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                {!! Form::label('pass','CONTRASEÑA')!!}
                                                <div class="input-group">
                                                    <div class="input-group-addon">                              
                                                    {!!Form::password('password', ['class' => 'form-control input-rounded', 'placeholder' => 'Contraseña' , 'id'=>'pwd2']) !!}
                                                 </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                    
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                       <!--  Second tab -->
                        <div class="tab-pane" id="suscripcion" role="tabpanel">
                                <div class="card-body">
                                    <div class="media">
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-package f-s-40 color-primary"></i></i><h4 class="card-title">Mi Suscripción</h4>
                                                <p style="color:grey;"><strong>{{$ordenes->nombre}}</strong></p><br>

                                                <p><strong>Detalles:</strong></p><br>
                                                <p>{{$ordenes->detalles}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-money f-s-40 color-primary"></i></i><h4 class="card-title">Facturación</h4>
                                                <a href="#">Ver facturas</a><br>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-files f-s-40 color-primary"></i></i><h4 class="card-title">Ordenes</h4>
                                                <a href="#">Ver Ordenes</a><br>
                                                <br>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Tercer Tab -->
                                <div class="tab-pane" id="empresa" role="tabpanel">
                                    <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información básica de la empresa</p>
                                {!! Form::open(['route' => 'gymadmin.empresas.update','files' => true ,'method' => 'PUT']) !!}



                                <div class="row p-t-20">
                                    
                                    <div class="col-sm-4">
                                        <label for="logo" >Logo Empresa</label>
                                        <img id="blah" class="logoempresa" src=""/> 
                                        <input type='file' name="logo" class="logocreate" onchange="readURL(this);" class="btn btn-small"/>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('sitioweb','Sitio Web')!!}
                                            {!!Form::text('sitioweb',$empresas->sitioweb, ['class' => 'form-control input-rounded', 'placeholder' => 'Sitio Web' , 'required']) !!}
                                         </div>
                                    </div>

                  
                                </div>   
                                <div class="row p-t-20">
                                    

                                    <div class="col-sm-3">
                                        <div class= "form-group">                              
                                        {!! Form::label('tipo_de_identificacion','TIPO DE IDENTIFICACIÓN')!!}
                                            {!!Form::select('tipo_de_identificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],[$empresas->tipo_de_identificacion],['class' => 'form-control input-rounded', 'id'=>'tipoid','required'])!!}
                                         </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('identificacion','IDENTIFICACIÓN')!!}
                                            {!!Form::number('identificacion',$empresa->identificacion, ['class' => 'form-control input-rounded', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required', 'min'=>'0', 'readonly']) !!}
                                         </div>

                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">                              
                                        {!! Form::label('dv','DV')!!}
                                            {!!Form::number('dv',$empresa->dv, ['class' => 'form-control input-rounded', 'id'=> 'dv', 'placeholder' => 'DV' , 'required', 'min'=>'0']) !!}
                                         </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('razon_social','RAZÓN SOCIAL')!!}
                                            {!!Form::text('razon_social',$empresa->razon_social, ['class' => 'form-control input-rounded', 'placeholder' => 'Razón Social' , 'required']) !!}
                                         </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('departamento','DEPARTAMENTO')!!}
                                            {!!Form::text('departamento',$empresa->departamento, ['class' => 'form-control input-rounded', 'placeholder' => 'departamento' , 'required']) !!}
                                         </div>
                                    </div> 

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('ciudad','CIUDAD')!!}
                                            {!!Form::text('ciudad',$empresa->ciudad, ['class' => 'form-control input-rounded', 'placeholder' => 'Ciudad' , 'required']) !!}
                                         </div>
                                    </div>    

                                    <div class="col-sm-5">
                                        <div class="form-group">                              
                                        {!! Form::label('direccion','DIRECCIÓN')!!}
                                            {!!Form::text('direccion',$empresa->direccion, ['class' => 'form-control input-rounded', 'placeholder' => 'Dirección' , 'required']) !!}
                                         </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('telefono','TÉLEFONO')!!}
                                            {!!Form::number('telefono',$empresa->telefono, ['class' => 'form-control input-rounded', 'placeholder' => 'Télefono' , 'required', 'min'=>'0']) !!}
                                         </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('emailfac','EMAIL FACTURACIÓN')!!}
                                            {!!Form::email('emailfac',$empresa->emailfac, ['class' => 'form-control input-rounded', 'placeholder' => 'Email Facturación' , 'required']) !!}
                                         </div>
                                    </div>
                                    


                                </div> 

                                        
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                                    <!-- Fin 3 columna -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>

                <!-- End PAge Content -->
            </div>

            @endsection