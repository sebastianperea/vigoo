@extends('template.gymadmin.main')
@section('titulo', 'Configuración')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                    <div class="media">
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-settings f-s-40 color-primary"></i></i><h4 class="card-title center">GENERAL</h4>
                                                <a href="{{ route('gymadmin.config.general') }}">Ir a General</a><br>
                                                <br>
                                                <p></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-money f-s-40 color-primary"></i></i><h4 class="card-title">Finanzas</h4>
                                                <a href="#">Ir a finanzas</a><br>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-files f-s-40 color-primary"></i></i><h4 class="card-title">Servicios</h4>
                                                <a href="#">Ir a servicios</a><br>
                                                <br>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->

            @endsection