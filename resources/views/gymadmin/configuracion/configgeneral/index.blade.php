@extends('template.gymadmin.main')
@section('titulo', 'Configuración')


@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                    <div class="media">
                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-bookmark f-s-40 color-primary"></i></i><h4 class="card-title center">Mi negocio</h4>
                                                <a href="{{ route('gymadmin.config.general.minegocio') }}">Ir a mi negocio</a><br>
                                                <br>
                                                <p></p>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-check f-s-40 color-primary"></i></i><h4 class="card-title">Suscripción a Vigo</h4>
                                                <a href="#">Ir a suscripción</a><br>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="card p-30">
                                                <i class="ti-user f-s-40 color-primary"></i></i><h4 class="card-title">Perfiles y usuarios</h4>
                                                <a href="#">Ir a perfiles y usuarios</a><br>
                                                <br>

                                            </div>
                                        </div>

                                        



                                    </div>
                                    <!--  Otro row -->

                                    <div class="row">
                                        <div class="col-md-4">

                                                    <div class="card p-30">
                                                        <i class="ti-flag-alt f-s-40 color-primary"></i></i><h4 class="card-title">Politicas</h4>
                                                        <a href="#">Ir a politicas</a><br>
                                                        <br>

                                                    </div>
                                                    
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="card p-30">
                                                        <i class="ti-flag-alt f-s-40 color-primary"></i></i><h4 class="card-title">Otros</h4>
                                                        <a href="#">Ir a otros</a><br>
                                                        <br>

                                                    </div>
                                                    
                                                </div>

                                 </div>

                                         

                        <!-- Fin otro row -->

                                </div>



                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>


                    

                    <!-- Column -->

            @endsection