@extends('template.gymadmin.main')
@section('titulo', 'Mi negocio')


@section('content')

    <div class="container-fluid">

    <div class="row bg-white m-l-0 m-r-0 box-shadow ">

                
                    <!-- column -->
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Mi negocio</h4>

                                
                                        <div class="avatar">
                                            <img src="" alt="Logo" />
                                        </div>

                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    
                    <div class="col-md-8">
                        <div class="row">
                            <div class="card">
                            
                              
                            {!! Form::open(['route' => 'gymadmin.config.general.minegocio.update','files' => true ,'method' => 'PUT', 'class' => 'form-inline']) !!}

                            <div class=".col-sm-1">
                                <div class="form-group">
                             {!! Form::label('name','Razón social')!!}

                              {!!Form::text('name', $empresas->razon_social,['class' => 'form-control input-rounded', 'placeholder' => 'Nombres y apellidos' , 'required']) !!}

                               
                                </div>
                            </div>

                            <div class=".col-sm-2">
                                <div class="form-group">

                                    {!! Form::label('tipo_de_identificacion','TIPO DE IDENTIFICACIÓN')!!}
                                            {!!Form::select('tipo_de_identificacion', ['Seleccione','NIT' => 'NIT','CC'=> 'CC', 'PAS' =>'PAS', 'CE' => 'CE'],[$empresas->tipo_de_identificacion],['class' => 'form-control input-rounded', 'id'=>'tipoid','required'])!!}
                    
                            </div>                       
                        </div>

                        </div>

                        
                        <div class=".col-sm-2">

                            <div class="form-group">
                            
                            {!! Form::label('identificacion','IDENTIFICACIÓN')!!}
                                            {!!Form::number('identificacion',$empresas->identificacion, ['class' => 'form-control input-rounded', 'id'=> 'identificacion', 'placeholder' => 'Identificación' , 'required', 'min'=>'0', 'readonly']) !!}
                                </div>
                        </div>

                        <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('dv','DV')!!}
                                            {!!Form::number('dv',$empresas->dv, ['class' => 'form-control input-sm input-rounded', 'id'=> 'dv', 'placeholder' => 'DV' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>

                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('Pagina web','Pagina web')!!}
                                            {!!Form::text('sitioweb',$empresas->sitioweb, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>

                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('Departamento','Departamento')!!}
                                            {!!Form::text('departamento',$empresas->departamento, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>

                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('Ciudad','Ciudad')!!}
                                            {!!Form::text('ciudad',$empresas->ciudad, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>


                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('Dirección','Dirección')!!}
                                            {!!Form::text('direccion',$empresas->direccion, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>

                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('Télefono','Telefono')!!}
                                            {!!Form::text('telefono',$empresas->telefono, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              

                          </div>
                      </div>

                      <div class=".col-sm-2">

                            <div class="form-group">
                     {!! Form::label('email','email')!!}
                                            {!!Form::text('email',$empresas->emailfac, ['class' => 'form-control input-sm input-rounded' , 'required', 'min'=>'0']) !!}


                              {!! Form::close ()!!}

                          </div>
                      </div>
                              
                                      
                        
                    </div>

                </div>
                    <!-- column -->
                
            </div>



            

            <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Sedes</h4>
                                
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Departamento</th>
                                                <th>Ciudad</th>
                                                <th>Dirección</th>
                                                <th>Télefono</th>
                                                <th>E-mail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tiger Nixon</td>
                                                <td>System Architect</td>
                                                <td>Edinburgh</td>
                                                <td>61</td>
                                                <td>2011/04/25</td>
                                                <td>$320,800</td>
                                            </tr>
                                            <tr>
                                                <td>Garrett Winters</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>63</td>
                                                <td>2011/07/25</td>
                                                <td>$170,750</td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection