@extends('template.gymadmin.main')
@section('titulo', 'Añadir resoluciones')


@section('content')


<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            {!! Form::open(['route' => 'resoluciones.store', 'method' => 'POST']) !!} 
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información Resoluciones</p>
                                <div class="row p-t-20">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('nombre','Nombre*')!!}
                                            {!!Form::text('nombre',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombre' , 'required']) !!}
                                         </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('prefijo','Prefijo')!!}
                                            {!!Form::text('prefijo',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Prefijo']) !!}
                                         </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('numero_inic','Numero Inicial*')!!}
                                            {!!Form::number('numero_inic',null, ['class' => 'form-control input-rounded', 'id'=> 'numero_inic', 'placeholder' => 'Numero Inicial' , 'required', 'min'=>'0']) !!}
                                         </div>

                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">                              
                                        {!! Form::label('numero_final','Numero Final*')!!}
                                            {!!Form::number('numero_final',null, ['class' => 'form-control input-rounded', 'id'=> 'numero_final', 'placeholder' => 'Numero Final' , 'required', 'min'=>'0']) !!}
                                         </div>

                                    </div>

                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('resolucion','Resolución')!!}
                                            {!!Form::textarea('resolucion',null, ['class' => '' , 'required','rows'=>'3']) !!}
                                         </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        {!! Form::label('preferida','Preferida')!!}                               
                                        {{ Form::radio('preferida', 'Si') }} Si
                                        {{ Form::radio('preferida', 'No') }} No
                                         </div>

                                    </div>


                                    

                                </div> 
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection            