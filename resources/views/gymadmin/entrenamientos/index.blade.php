@extends('template.gymadmin.main')

@section('titulo', 'Entrenamientos')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Entrenamientos</h4>
                                <h6 class="card-subtitle">Todos los entrenamientos</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>FECHA CREACIÓN</th>
                                                <th>ULTIMA ACTUALIZACIÓN</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($entrenamientos as $entrenamiento)
                                            <tr>
                                                <td>{{$entrenamiento->id}}</td>
                                              <td>{{$entrenamiento->nombre}}</td>
                                              <td>{{$entrenamiento->created_at}}</td>
                                              <td>{{$entrenamiento->updated_at}}</td>
                                              <td><a href="{{ route('entrenamientos.edit', $entrenamiento->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('gymadmin.entrenamientos.destroy' , $entrenamiento->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
