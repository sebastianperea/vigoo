@extends('template.gymadmin.main')

@section('titulo', 'Ingresos')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ingresos</h4>
                                <h6 class="card-subtitle">Todos los ingresos</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>FECHA CREACIÓN</th>
                                                <th>ULTIMA ACTUALIZACIÓN</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ingresos as $ingreso)
                                            <tr>
                                                <td>{{$ingreso->id}}</td>
                                              <td>{{$ingreso->nombre}}</td>
                                              <td>{{$ingreso->created_at}}</td>
                                              <td>{{$ingreso->updated_at}}</td>
                                              <td><a href="{{ route('ingresos.edit', $ingreso->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('gymadmin.ingresos.destroy' , $ingreso->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
