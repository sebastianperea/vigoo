@extends('template.gymadmin.main')

@section('titulo', 'Servicios')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Servicios</h4>
                                <h6 class="card-subtitle">Todos los servicios</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>DIAS ASIGNADOS</th>
                                                <th>FECHA CREACIÓN</th>
                                                <th>ULTIMA ACTUALIZACIÓN</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($servicios as $servicio)
                                            <tr>
                                                <td>{{$servicio->id}}</td>
                                              <td>{{$servicio->nombre}}</td>
                                              <td>{{$servicio->dias}}</td>
                                              <td>{{$servicio->created_at}}</td>
                                              <td>{{$servicio->updated_at}}</td>
                                              <td><a href="{{ route('servicios.edit', $servicio->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('gymadmin.servicios.destroy' , $servicio->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
