@extends('template.gymadmin.main')
@section('titulo', 'Editar Servicios')


@section('content')


<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            {!! Form::open(['route' =>['gymadmin.servicios.update', $servicios->id] , 'method' => 'PUT']) !!} 
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información Servicios</p>
                                <div class="row p-t-20">
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('nombre','NOMBRE')!!}
                                            {!!Form::text('nombre',$servicios->nombre, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombre' , 'required']) !!}
                                         </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class= "form-group">                              
                                        {!! Form::label('Personal','Personal')!!}
                                            {!!Form::select('tipo_de_identificacion', ['Seleccione','Pepito Perez' => 'Pepito Perez'],null,['class' => 'form-control input-rounded', 'id'=>'personal','required'])!!}
                                         </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <div class= "form-group">                              
                                        {!! Form::label('tasadepago','Tasa de pago')!!}
                                            {!!Form::select('tasadepago', ['Seleccione'],null,['class' => 'form-control input-rounded', 'id'=>'tasadepago','required'])!!}
                                         </div>
                                    </div>

                                </div>

                                <hr>

                                <h4 class="card-title">Horarios</h4><br>

                                <div class="col-sm-5 " align="center">
                                        <div class= "form-group">                              
                                        <div class="btn-group" data-toggle="buttons">
            
                                            <label class="btn btn-success active">
                                                L
                                                <input name='dias[]' type="checkbox" autocomplete="off" checked value="L">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>

                                            <label class="btn btn-primary">
                                                M
                                                <input type="checkbox" name="dias[]"autocomplete="off" value="M">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>            
                                        
                                            <label class="btn btn-info">
                                                M
                                                <input type="checkbox" name="dias[]" autocomplete="off" value="X">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>            
                                        
                                            <label class="btn btn-primary">
                                                J
                                                <input type="checkbox" name="dias[]" autocomplete="off" value="J">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>            

                                            <label class="btn btn-warning">
                                                V
                                                <input type="checkbox"  name="dias[]"autocomplete="off" value="V">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>            
                                        
                                            <label class="btn btn-success">
                                                S
                                                <input type="checkbox" name="dias[]" autocomplete="off" value="S">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label> 
                                            <label class="btn btn-danger">
                                                D
                                                <input type="checkbox" name="dias[]" autocomplete="off" value="D">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </label>            
                                        
                                        </div>
                                         </div>
                                         <div class="row p-t-20">

                                         <div class="col-sm-5">
                                            <div class= "form-group">                              
                                        {!! Form::label('hora_desde','Hora desde')!!}
                                            {!!Form::time('hora_desde',$servicios->hora_desde,['class' => 'form-control input-rounded', 'id'=>'horadesde','required'])!!}
                                         </div> 
                                          </div>
                                          <div class="col-sm-5">
                                            <div class= "form-group">                              
                                        {!! Form::label('hora_hasta','Hora Hasta')!!}
                                            {!!Form::time('hora_hasta',$servicios->hora_hasta,['class' => 'form-control input-rounded', 'id'=>'tasadepago','required'])!!}
                                         </div>
                                          </div>
                                        </div>
                                        <div class="row p-t-20">

                                         <div class="col-sm-5">
                                            <div class= "form-group">                              
                                        {!! Form::label('fecha_inic','Fecha Inicial')!!}
                                            {!!Form::date('fecha_inic',$servicios->fecha_inic,['class' => 'form-control input-rounded', 'id'=>'fecha_inic','required', 'step'=>'0.01'])!!}
                                         </div> 
                                          </div>
                                          <div class="col-sm-5">
                                            <div class= "form-group">                              
                                        {!! Form::label('fecha_final','Fecha Final')!!}
                                            {!!Form::date('fecha_final',$servicios->fecha_final,['class' => 'form-control input-rounded', 'id'=>'fecha_final','required'])!!}
                                         </div>
                                          </div>

                                          
                                        </div>
                                    </div> 
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection            