@extends('template.gymadmin.main')
@section('titulo', 'Egresos')


@section('content')


<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            {!! Form::open(['route' => 'egresos.store', 'method' => 'POST']) !!} 
                                <h4 class="card-title">Información Básica</h4>
                                <p class="text-muted m-b-15 f-s-12">Información Entrenamientos</p>
                                <div class="row p-t-20">
                                    <div class="col-sm-4">
                                        <div class="form-group">                              
                                        {!! Form::label('nombre','NOMBRE')!!}
                                            {!!Form::text('nombre',null, ['class' => 'form-control input-rounded', 'placeholder' => 'Nombre' , 'required']) !!}
                                         </div>
                                    </div> 

                                </div> 
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
     
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>

@endsection            