@extends('template.gymadmin.main')

@section('titulo', 'Egresos')
@section('content')

<div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
						<div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Egresos</h4>
                                <h6 class="card-subtitle">Todos los egresos</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>FECHA CREACIÓN</th>
                                                <th>ULTIMA ACTUALIZACIÓN</th>
                                                <th>ACCIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($egresos as $egreso)
                                            <tr>
                                                <td>{{$egreso->id}}</td>
                                              <td>{{$egreso->nombre}}</td>
                                              <td>{{$egreso->created_at}}</td>
                                              <td>{{$egreso->updated_at}}</td>
                                              <td><a href="{{ route('egresos.edit', $egreso->id) }}" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"> </a><a href="{{route('gymadmin.egresos.destroy' , $egreso->id )}}" target="" class="btn btn-danger btn-xs" ><span class="fa fa-trash"> </a> </td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
