<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('admin','Admin\AdminController', ['middleware' => ['auth', 'admin'],'except' => ['show']]);

Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function () {


	Route::resource('empresas','Admin\EmpresasAdminController', ['except' => ['show']]);

    Route::resource('manage_entrenamientos','Admin\EntrenamientosController', ['except' => ['show','create']]);
    
	Route::resource('planes','Admin\PlanesAdminController', ['except' => ['show']]);
	 Route::get('{id}/destroy', 
    [ 'uses' => 'Admin\PlanesAdminController@destroy',
    'as' => 'admin.planes.destroy']);

	 Route::get('uploadedFile/{filename}',['uses' => 'Admin\EmpresasAdminController@getFile',
    'as' => 'viewfiles']);

	 Route::resource('facturas','Admin\FacturasAdminController', ['except' => ['show']]);


	 
    Route::resource('ordenes','Admin\OrdenesController', ['except' => ['show']]);

    Route::get('ordenes/{id}',array('as'=>'ordenes.show','uses'=>'Admin\OrdenesController@show'));




});



Route::resource('gymadmin','Gymadmin\GymAdminController', ['middleware' => ['auth', 'gymadmin'],'except' => ['show']]);

Route::group(['prefix' => 'gymadmin','middleware' => ['auth', 'gymadmin']], function () {

	Route::get('perfil',['uses' => 'Gymadmin\ProfileController@index',
    'as' => 'gymadmin.profile.index']);

	Route::get('myuser',['uses' => 'Gymadmin\UsersController@edit',
    'as' => 'gymadmin.myuser.edit']);
    Route::put('myuser/update',['uses' => 'Gymadmin\UsersController@update',
    'as' => 'gymadmin.myuser.update']);
    Route::put('empresas/update',['uses' => 'Gymadmin\ProfileController@updateempresas',
    'as' => 'gymadmin.empresas.update']);

    Route::resource('entrenamientos','Gymadmin\EntrenamientosController', ['except' => ['show']]);


    Route::put('entrenamientos/{id}/update',['uses' => 'Gymadmin\EntrenamientosController@update',
    'as' => 'gymadmin.entrenamientos.update']);
    Route::get('entrenamientos/{id}/destroy', 
    [ 'uses' => 'Gymadmin\EntrenamientosController@destroy',
    'as' => 'gymadmin.entrenamientos.destroy']);

   
    Route::resource('servicios','Gymadmin\ServiciosController', ['except' => ['show']]);
    Route::get('servicios/{id}/destroy', 
    [ 'uses' => 'Gymadmin\ServiciosController@destroy',
    'as' => 'gymadmin.servicios.destroy']);

    Route::put('servicios/{id}/update',['uses' => 'Gymadmin\ServiciosController@update',
    'as' => 'gymadmin.servicios.update']);

    Route::resource('ingresos','Gymadmin\IngresosController', ['except' => ['show']]);
    Route::get('ingresos/{id}/destroy', 
    [ 'uses' => 'Gymadmin\IngresosController@destroy',
    'as' => 'gymadmin.ingresos.destroy']);

    Route::put('ingresos/{id}/update',['uses' => 'Gymadmin\IngresosController@update',
    'as' => 'gymadmin.ingresos.update']);

    Route::resource('egresos','Gymadmin\EgresosController', ['except' => ['show']]);
    Route::get('egresos/{id}/destroy', 
    [ 'uses' => 'Gymadmin\EgresosController@destroy',
    'as' => 'gymadmin.egresos.destroy']);

    Route::put('egresos/{id}/update',['uses' => 'Gymadmin\EgresosController@update',
    'as' => 'gymadmin.egresos.update']);

    Route::resource('resoluciones','Gymadmin\ResolucionesController', ['except' => ['show']]);
    Route::get('resoluciones/{id}/destroy', 
    [ 'uses' => 'Gymadmin\ResolucionesController@destroy',
    'as' => 'gymadmin.resoluciones.destroy']);

    Route::put('resoluciones/{id}/update',['uses' => 'Gymadmin\ResolucionesController@update',
    'as' => 'gymadmin.resoluciones.update']);

    Route::get('configuracion',['uses' => 'Gymadmin\ConfigController@index',
    'as' => 'gymadmin.config.index']);

    Route::group(['prefix' => 'gymadmin','middleware' => ['auth', 'gymadmin']], function () {

        Route::get('configuracion_general',['uses' => 'Gymadmin\ConfigController@configgeneral',
    'as' => 'gymadmin.config.general']);

        Route::get('mi_negocio',['uses' => 'Gymadmin\ConfigController@minegocio',
    'as' => 'gymadmin.config.general.minegocio']);

        Route::put('mi_negocio/update',['uses' => 'Gymadmin\ConfigController@minegocioup',
    'as' => 'gymadmin.config.general.minegocio.update']);

    });

    // Route::resource('facturas','Gymadmin\FacturasGymadminController', ['except' => ['show']]);
   

});


Route::get('/logout',
 'Auth\LoginController@logout');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
