<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class servicios extends Model
{
    protected $table="servicios";	
   	protected $fillable= ['id','nombre','dias','hora_desde','hora_hasta','fecha_inic','fecha_final','identificacion'];

   	
   	public function empresas()
   {

  return $this->belongsToMany('App\empresas','identificacion');

   }
}
