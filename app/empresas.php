<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empresas extends Model
{
    protected $table="empresas";	
    protected $primaryKey = 'identificacion';
   	protected $fillable= ['identificacion','tipodepersona','tipo_de_identificacion','dv','razon_social','departamento','ciudad','direccion','telefono', 'emailfac','logo','sitioweb','estado'];

   public function planes()
   {

      return $this->hasMany('App\planes','id_plan');

   }

    public function facturas()
   {

	return $this->belongsTo('App\facturas','id');

   }

    public function ordenes()
   {

  return $this->belongsToMany('App\ordenes','id');

   }
   
   public function entrenamientos()
    {
        return $this->hasMany('App\identificacion');
    }

    public function servicios()
    {
        return $this->hasMany('App\identificacion');
    }
}
