<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class entrenamientos extends Model
{
    protected $table="entrenamientos";	
   	protected $fillable= ['id','nombre','identificacion'];

   	public function empresas()
   {

  return $this->belongsToMany('App\empresas','identificacion');

   }
}
