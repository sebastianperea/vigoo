<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resoluciones extends Model
{
    protected $table="resoluciones";
   protected $fillable= ['id','resolucion','prefijo','nombre','numero_inic','numero_final','preferida','estado','identificacion'];
}
