<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingresos extends Model
{
     protected $table="ingresos";	
   	protected $fillable= ['id','nombre','identificacion'];
}
