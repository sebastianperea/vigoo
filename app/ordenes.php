<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ordenes extends Model
{
    protected $table="ordenes";	
   	protected $fillable= ['id','id_plan','identificacion','fecha_inic','fecha_venc','estado','estado_servicio'];

   	public function empresas()
   {

      return $this->hasMany('App\empresas','identificacion');

   }

   public function planes()
   {

      return $this->hasMany('App\planes');

   }
}
