<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\planes;
use App\empresas;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class EmpresasAdminController extends Controller
{

	public function index (Request $request){

		$empresas= empresas::orderBy('identificacion' , 'ASC')->get();

		       
		  	  	return view ('admin.empresas.index')->with('empresas', $empresas);
		 
		  }

		    public function create(Request $request){
		    	$planes = planes::orderBy('id' , 'ASC')->pluck('nombre','id');
		    	
		    return view('admin.empresas.create')->with('planes', $planes);
		                                     

		  }


		  public function store(Request $request){

		  			
		 			$logo = $request->file('logo');
              		$namefpn = 'Fitsoft_'.time().rand(1,99999).'.'.$logo->getClientOriginalExtension();
                    $path = Storage_path() . '/files/empresas/';
              		$logo->move($path, $namefpn);
				   $empresas= new empresas($request->all());
				   $empresas->logo = $namefpn;
				   $empresas->save();
				   $users= new User($request->all());
				   $users->password = Hash::make($request->password);
				   $users->save();

					Flash::success("Se ha registrado la empresa ". $empresas->razon_social. " de forma existosa");
				    return back();
				    
				  	
				  }


			 public function edit($identificacion){


			 	$empresa= empresas::find($identificacion);


		  		$empresas= empresas::find($identificacion);
		  		
		  		$user = User::where('identificacion','=',$identificacion)->first();

		  		return view('admin.empresas.edit')
		  		->with('empresas', $empresas)
		  		->with('user',$user);
		  		
    		}

    	public function update(Request $request, $identificacion){


		  		$empresas= empresas::find($identificacion);
		  		$empresas->tipo_de_identificacion = $request->tipo_de_identificacion;
		  		$empresas->identificacion = $request->identificacion;
		  		$empresas->dv = $request->dv;
		  		$empresas->razon_social = $request->razon_social;
		  		$empresas->departamento = $request->departamento;
		  		$empresas->ciudad = $request->ciudad;
		  		$empresas->departamento = $request->departamento;
		  		$empresas->direccion = $request->direccion;
		  		$empresas->telefono = $request->telefono;
		  		$empresas->emailfac = $request->emailfac;
		  		$empresas->save();



		  		$users = User::where('identificacion','=', $identificacion)->first();
		  		$users->name = $request->name;
		  		$users->email = $request->email;

		  		if ($request->password == '') {
		  			$users->save();
		  		}

		  		else{
		  			$users->password = Hash::make($request->password);
		  			$users->save();
		  		}
		  		
		  		 Flash::success('La empresa'.$empresas->razon_social. 'ha sido actualizada con exito');
		  		return back();
   

    		}
}
