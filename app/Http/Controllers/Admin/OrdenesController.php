<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\empresas;
use App\planes;
use App\facturas;
use App\ordenes;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class OrdenesController extends Controller
{
  public function show (Request $request, $id)
   {


    $ordenes= DB::table('ordenes')
                ->join('empresas', 'ordenes.identificacion', '=', 'empresas.identificacion')
                ->select('ordenes.*','empresas.razon_social')
                ->orderBy('id','ASC')->where('ordenes.id','=',$id)->first();

    $planes= DB::table('ordenes')
                ->join('planes', 'ordenes.id_plan', '=', 'planes.id')
                ->select('ordenes.*','planes.nombre', 'planes.detalles', 'planes.precio')
                ->orderBy('id','ASC')->where('ordenes.id','=',$id)->first();

    return view ('admin.ordenes.show')
    ->with('ordenes', $ordenes)
    ->with('planes', $planes);

  }
	public function index (Request $request)
   {

   	$ordenes= DB::table('ordenes')
                ->join('empresas', 'ordenes.identificacion', '=', 'empresas.identificacion')
                ->select('ordenes.*','empresas.razon_social')
                ->orderBy('id','ASC')->get();

   	return view ('admin.ordenes.index')
   	->with('ordenes', $ordenes);

	}
   public function create (Request $request)
   {

   	$empresas = empresas::orderBy('identificacion' , 'ASC')->pluck('razon_social','identificacion');
   	$planes = planes::orderBy('id' , 'ASC')->pluck('nombre','id');
   	return view ('admin.ordenes.create')
   	->with('empresas', $empresas)
   	->with('planes', $planes);

	}

	public function store (Request $request)
   {

	   	$ordenes= new ordenes($request->all());
			    
			    $ordenes->save();
				Flash::success("Se ha registrado la orden #  ". $ordenes->id. " de forma existosa");
			    return back();

	}
}
