<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use App\planes;


class PlanesAdminController extends Controller
{

		public function index (Request $request){

		        $planes= planes::orderby('id', 'ASC')->get();
		  	  	return view ('admin.planes.index')->with('planes', $planes);
		 
		  }
		   public function create(Request $request){

		    return view('admin.planes.create');
		 
		  }

		  public function store(Request $request){

		   $planes= new planes($request->all());
		    
		    $planes->save();

			Flash::success("Se ha registrado el plan ". $planes->nombre. " de forma existosa");
		    return back();
		    
		  	
		  }

		  public function edit($id){

		  		$planes= planes::find($id);
		  		return view('admin.planes.edit')->with('planes', $planes);
   

    		}

    		public function update(Request $request, $id){

		  		$planes= planes::find($id);
		  		$planes->nombre = $request->nombre;
		  		$planes->precio = $request->precio;
		  		$planes->detalles = $request->detalles;
		  		$planes->save();
		  		 Flash::success('El plan ha sido actualizado con exito');
		  		return back();
   

    		}

    		public function destroy($id){

    				
		  		$plan= planes::find($id);
		  		$plan->delete();
		  		Flash::warning('El plan '.$plan->id.' ha sido borrado');
		  		return redirect()->route('planes.index');
		  		 
    		}
}
