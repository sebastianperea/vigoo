<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\empresas;
use App\planes;
use App\facturas;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class FacturasAdminController extends Controller
{


  public function index (Request $request){

     $facturas= DB::table('facturas')
                
                ->join('empresas', 'facturas.identificacion', '=', 'empresas.identificacion')
               
                ->select('facturas.*','empresas.razon_social')
                ->orderBy('id','ASC')->get();

                
            return view ('admin.facturas.index')->with('facturas',$facturas);
     
      }

    public function create(Request $request){

    			$facturas= facturas::all();
    			$lastfactura = $facturas->last();

          if ($lastfactura == null) {
            $numerofactura = 1;
          }

          else{
            $numerofactura = $lastfactura->id + 1 ;
          }

		    	$empresas = empresas::orderBy('identificacion' , 'ASC')->pluck('razon_social','identificacion');

		    	$planes = planes::orderBy('id' , 'ASC')->pluck('nombre','id');
		    	return view('admin.facturas.create')
		    	->with('empresas', $empresas)
		    	->with('planes', $planes)
		    	->with('numerofactura', $numerofactura);

                                            
		  }


		   public function autocompletesa(Request $request){
               
                          $term=$request->term;

                          $data = planes::where('nombre','LIKE','%'.$term.'%')
                          ->take(10)
                          ->get();
                          
                          $results = array();
                          foreach ($data as $key => $v) {
                          
                            $results[]=['producto'=>$v->value,'precio'=>$v->precio];
                          }
                            return response()->json($results);
                        }


      public function store(Request $request){
        
                  
              $facturas= new facturas($request->all());
              $facturas->save();
              
                    

              $extra = array_map(function($precio, $cantidad){
                               return ['cantidad' => $cantidad, 'precio' => $precio ];
                                }, $request->preciou_ , $request->cantidad_);

                              $data = array_combine($request->producto_, $extra);  

                              

                              $facturas->planes()->sync($data);

                              Flash::success("Se ha registrado la factura". $facturas->id. " de forma existosa"); 

                              return back();      
  }

}
