<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\entrenamientos;
use App\User;
use App\empresas;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntrenamientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $entrenamientos= DB::table('entrenamientos')
                
                ->join('empresas', 'entrenamientos.identificacion', '=', 'empresas.identificacion')
                
                ->select('entrenamientos.*','empresas.razon_social')->where('entrenamientos.default','=', 1)
                ->orderBy('id','ASC')->get();
         return view('admin.entrenamientos.index')
         ->with('entrenamientos', $entrenamientos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $this->validate($request, [
      //       'nombre' => 'required'
      //   ]);
      //   entrenamientos::create($request->all());
      //   return;


        dd($request->all());
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
