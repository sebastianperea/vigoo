<?php

namespace App\Http\Controllers\Gymadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\empresas;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Gymadmin\MyuserRequest;

class ProfileController extends Controller
{
    public function index (Request $request){

        $identificacion= Auth::user()->identificacion;

        $ordenes= DB::table('ordenes')
                
                ->join('empresas', 'ordenes.identificacion', '=', 'empresas.identificacion')
                ->join('planes', 'ordenes.id_plan', '=', 'planes.id')
               
                ->select('ordenes.*','empresas.razon_social', 'planes.nombre', 'planes.detalles', 'empresas.identificacion')->where('ordenes.identificacion', '=',$identificacion)
                ->orderBy('id','ASC')->first();
               
    	$empresa = DB::table('users')
                
                ->join('empresas', 'users.identificacion', '=', 'empresas.identificacion')
               
                ->select('empresas.*','razon_social')
                ->orderBy('id','ASC')->first();

        $empresas = empresas::find($identificacion);

    	 $user= Auth::user()->id;
    	 $users = User::find($user);


    	 
		return view ('gymadmin.perfil.index')
		->with('users', $users)
		->with('empresa', $empresa)
        ->with('ordenes', $ordenes)
        ->with('empresas', $empresas);
		 
	}

    public function updateempresas (Request $request){

        $identificacion= Auth::user()->identificacion;

        $empresas= empresas::find($identificacion);
                $empresas->tipo_de_identificacion = $request->tipo_de_identificacion;
                $empresas->identificacion = $request->identificacion;
                $empresas->dv = $request->dv;
                $empresas->razon_social = $request->razon_social;
                $empresas->departamento = $request->departamento;
                $empresas->ciudad = $request->ciudad;
                $empresas->departamento = $request->departamento;
                $empresas->direccion = $request->direccion;
                $empresas->telefono = $request->telefono;
                $empresas->emailfac = $request->emailfac;
                $empresas->save();


         
            Flash::success('La empresa '.$empresas->razon_social. ' ha sido actualizada con exito');
                return back();
    }
}
