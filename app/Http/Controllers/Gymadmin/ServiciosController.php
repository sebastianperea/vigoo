<?php

namespace App\Http\Controllers\Gymadmin;
use App\Http\Controllers\Controller;
use App\entrenamientos;
use App\User;
use App\servicios;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    public function index(Request $request)
    {
    	$identificacion = Auth::user()->identificacion;

		
		$servicios= DB::table('servicios')
                ->join('empresas', 'servicios.identificacion', '=', 'empresas.identificacion')
                ->select('servicios.*','empresas.razon_social')->where('servicios.identificacion','=', $identificacion)
                ->orderBy('id','ASC')->get();
		 return view('gymadmin.servicios.index')
		 ->with('servicios', $servicios);
		                                     
	}
	public function create(Request $request)
    {
			
	 return view('gymadmin.servicios.create');
		                                     

	}
	public function store(Request $request){

		

			
	 $servicios= new servicios($request->all());
	 $array = implode(', ', $request->dias);
	 $servicios->dias = $array;
	 $servicios->identificacion = Auth::user()->identificacion;
     $servicios->save();

     Flash::success("Se ha registrado el servicio". $servicios->nombre. " de forma existosa");

     return back();
		                                     

	}

	public function edit(Request $request, $id){
		$identificacion = Auth::user()->identificacion;
		$servicios = servicios::find($id);


		if ($servicios->identificacion == $identificacion ) {

			return view('gymadmin.servicios.edit')
			->with('id', $id)
			->with('servicios', $servicios);
		}

		else{
	 
			abort(404);                                
	}
		}


		public function update(Request $request, $id){


		$identificacion = Auth::user()->identificacion;
		$servicios = servicios::find($id);




		if ($servicios->identificacion == $identificacion ) {

			$servicios->nombre = $request->nombre;
			$servicios->save();

			 Flash::success("Se ha actualizado el entrenamiento ". $servicios->id. " de forma existosa"); 

              return back();      
		}

		else{
	 
			abort(404);                                
	}
		}


		public function destroy($id){

				$identificacion = Auth::user()->identificacion;
				$servicios= servicios::find($id);
    				
		  		

			  if ($servicios->identificacion == $identificacion ) {
				$servicios= servicios::find($id);
			  		$servicios->delete();
			  		Flash::warning('El entrenamiento '.$servicios->id.' ha sido borrado');
			  		return back();    
				}

			else{
		 
				abort(404);                                
				}	 
    	}
}
