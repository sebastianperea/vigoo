<?php

namespace App\Http\Controllers\Gymadmin;
use App\empresas;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
   
   public function index(Request $request)
    {
    	return view('gymadmin.configuracion.index');
	}

	public function configgeneral(Request $request)
    {
    	return view('gymadmin.configuracion.configgeneral.index');
	}

	public function minegocio(Request $request)
    {
    	$user= Auth::user()->identificacion;
    	$empresas= empresas::find($user);
    	return view('gymadmin.configuracion.configgeneral.minegocio.index')->with('empresas', $empresas);
	}
		                                     
	} 
