<?php

namespace App\Http\Controllers\Gymadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Gymadmin\userrequest;

class UsersController extends Controller
{
    public function edit (Request $request){

    	 $user= Auth::user()->id;
    	 $users = User::find($user);
    	 
		return view ('gymadmin.myuser.edit')
		->with('users', $users);
		 
	}
	public function update (userrequest $request){

		$users= User::find(Auth::user()->id);
		
		  	$users->name = $request->name;
		  	$users->email = $request->email;

		  	if ($request->password == '') {
		  		$users->save();
		  	}

		  	else{
		  		$users->password = Hash::make($request->password);
		  		$users->save();
		  	}
		Flash::success('Tu usuario '.$users->name. ' ha sido actualizada con exito');
		  		return back();
		 	
	}



}
