<?php

namespace App\Http\Controllers\Gymadmin;
use App\Http\Controllers\Controller;
use App\entrenamientos;
use App\User;
use App\resoluciones;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;


class ResolucionesController extends Controller
{
    public function create(Request $request)
    {	
	 return view('gymadmin.resoluciones.create');                                     
	}

	public function store(Request $request){
			
	 $resoluciones= new resoluciones($request->all());
	 $resoluciones->identificacion = Auth::user()->identificacion;
     $resolucioneses->save();

     Flash::success("Se ha registrado la resolución ". $resoluciones->nombre. " de forma existosa");

     return back();
		                                     

	}

	public function index(Request $request)
    {
    	$identificacion = Auth::user()->identificacion;

		$resoluciones= DB::table('resoluciones')
                ->join('empresas', 'resoluciones.identificacion', '=', 'empresas.identificacion')
                ->select('resoluciones.*','empresas.razon_social')->where('resoluciones.identificacion','=', $identificacion)
                ->orderBy('id','ASC')->get();
		 return view('gymadmin.resoluciones.index')
		 ->with('resoluciones', $resoluciones);
		                                     
	}

	public function edit(Request $request, $id){
		$identificacion = Auth::user()->identificacion;
		$resoluciones = resoluciones::find($id);


		if ($resoluciones->identificacion == $identificacion ) {

			return view('gymadmin.resoluciones.edit')
			->with('id', $id)
			->with('resoluciones', $resoluciones);
		}

		else{
	 
			abort(404);                                
	}
		}


		public function update(Request $request, $id){


		$identificacion = Auth::user()->identificacion;
		$resoluciones = resoluciones::find($id);




		if ($resoluciones->identificacion == $identificacion ) {

			$resoluciones->nombre = $request->nombre;
			$resoluciones->save();

			 Flash::success("Se ha actualizado el Ingreso ". $resoluciones->id. " de forma existosa"); 

              return back();      
		}

		else{
	 
			abort(404);                                
	}
		}


		public function destroy($id){

				$identificacion = Auth::user()->identificacion;
				$resoluciones= resoluciones::find($id);
    				
		  		

			  if ($resoluciones->identificacion == $identificacion ) {
				$resoluciones= resoluciones::find($id);
			  		$resoluciones->delete();
			  		Flash::warning('El Ingreso '.$resoluciones->id.' ha sido borrado');
			  		return back();    
				}

			else{
		 
				abort(404);                                
				}	 
    	}
}
