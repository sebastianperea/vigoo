<?php

namespace App\Http\Controllers\Gymadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\egresos;
use App\User;
use App\empresas;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

class EgresosController extends Controller
{
   public function index(Request $request)
    {
    	$identificacion = Auth::user()->identificacion;
		
		$egresos= DB::table('egresos')
                
                ->join('empresas', 'egresos.identificacion', '=', 'empresas.identificacion')
                
                ->select('egresos.*','empresas.razon_social')->where('egresos.identificacion','=', $identificacion)
                ->orderBy('id','ASC')->get();
		 return view('gymadmin.egresos.index')
		 ->with('egresos', $egresos);
		                                     
	}

    public function create(Request $request)
    {
			
	 return view('gymadmin.egresos.create');
		                                     

	}

	public function store(Request $request){
			
	 $egresos= new egresos($request->all());
	 $egresos->identificacion = Auth::user()->identificacion;
     $egresos->save();

     Flash::success("Se ha registrado el Egreso ". $egresos->nombre. " de forma existosa");

     return back();
		                                     

	}

	public function edit(Request $request, $id){
		$identificacion = Auth::user()->identificacion;
		$egresos = egresos::find($id);



		if ($egresos->identificacion == $identificacion ) {

			return view('gymadmin.egresos.edit')
			->with('id', $id)
			->with('egresos', $egresos);
		}

		else{
	 
			abort(404);                                
	}
		}


		public function update(Request $request, $id){


		$identificacion = Auth::user()->identificacion;
		$egresos = egresos::find($id);




		if ($egresos->identificacion == $identificacion ) {

			$egresos->nombre = $request->nombre;
			$egresos->save();

			 Flash::success("Se ha actualizado el Egreso ". $egresos->id. " de forma existosa"); 

              return back();      
		}

		else{
	 
			abort(404);                                
	}
		}


		public function destroy($id){

				$identificacion = Auth::user()->identificacion;
				$egresos= egresos::find($id);
    				
		  	  if ($egresos->identificacion == $identificacion ) {
				$egresos= egresos::find($id);
			  		$egresos->delete();
			  		Flash::warning('El Egreso '.$egresos->id.' ha sido borrado');
			  		return back();    
				}

			else{
		 
				abort(404);                                
				}	 
    	}
}
