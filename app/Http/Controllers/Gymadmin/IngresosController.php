<?php

namespace App\Http\Controllers\Gymadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ingresos;
use App\User;
use App\empresas;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

class IngresosController extends Controller
{
   public function index(Request $request)
    {
    	$identificacion = Auth::user()->identificacion;

		$ingresos= DB::table('ingresos')
                ->join('empresas', 'ingresos.identificacion', '=', 'empresas.identificacion')
                ->select('ingresos.*','empresas.razon_social')->where('ingresos.identificacion','=', $identificacion)
                ->orderBy('id','ASC')->get();
		 return view('gymadmin.ingresos.index')
		 ->with('ingresos', $ingresos);
		                                     
	}
	public function create(Request $request)
    {
			
	 return view('gymadmin.ingresos.create');
		                                     

	}
	public function store(Request $request){
			
	 $ingresos= new ingresos($request->all());
	 $ingresos->identificacion = Auth::user()->identificacion;
     $ingresos->save();

     Flash::success("Se ha registrado el Ingreso ". $ingresos->nombre. " de forma existosa");

     return back();
		                                     

	}

	public function edit(Request $request, $id){
		$identificacion = Auth::user()->identificacion;
		$ingresos = ingresos::find($id);


		if ($ingresos->identificacion == $identificacion ) {

			return view('gymadmin.ingresos.edit')
			->with('id', $id)
			->with('ingresos', $ingresos);
		}

		else{
	 
			abort(404);                                
	}
		}


		public function update(Request $request, $id){


		$identificacion = Auth::user()->identificacion;
		$ingresos = ingresos::find($id);




		if ($ingresos->identificacion == $identificacion ) {

			$ingresos->nombre = $request->nombre;
			$ingresos->save();

			 Flash::success("Se ha actualizado el Ingreso ". $ingresos->id. " de forma existosa"); 

              return back();      
		}

		else{
	 
			abort(404);                                
	}
		}


		public function destroy($id){

				$identificacion = Auth::user()->identificacion;
				$ingresos= ingresos::find($id);
    				
		  		

			  if ($ingresos->identificacion == $identificacion ) {
				$ingresos= ingresos::find($id);
			  		$ingresos->delete();
			  		Flash::warning('El Ingreso '.$ingresos->id.' ha sido borrado');
			  		return back();    
				}

			else{
		 
				abort(404);                                
				}	 
    	}
}
