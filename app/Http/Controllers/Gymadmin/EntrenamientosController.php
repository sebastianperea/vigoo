<?php

namespace App\Http\Controllers\Gymadmin;

use App\Http\Controllers\Controller;
use App\entrenamientos;
use App\User;
use App\empresas;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntrenamientosController extends Controller
{
	
	public function index(Request $request)
    {
    	$identificacion = Auth::user()->identificacion;
		
		$entrenamientos= DB::table('entrenamientos')
                
                ->join('empresas', 'entrenamientos.identificacion', '=', 'empresas.identificacion')
                
                ->select('entrenamientos.*','empresas.razon_social')->where('entrenamientos.identificacion','=', $identificacion)
                ->orderBy('id','ASC')->get();
		 return view('gymadmin.entrenamientos.index')
		 ->with('entrenamientos', $entrenamientos);
		                                     
	}

    public function create(Request $request)
    {
			
	 return view('gymadmin.entrenamientos.create');
		                                     

	}

	public function store(Request $request){
			
	 $entrenamientos= new entrenamientos($request->all());
	 $entrenamientos->identificacion = Auth::user()->identificacion;
     $entrenamientos->save();

     Flash::success("Se ha registrado el entrenamiento ". $entrenamientos->nombre. " de forma existosa");

     return back();
		                                     

	}

	public function edit(Request $request, $id){
		$identificacion = Auth::user()->identificacion;
		$entrenamientos = entrenamientos::find($id);



		if ($entrenamientos->identificacion == $identificacion ) {

			return view('gymadmin.entrenamientos.edit')
			->with('id', $id)
			->with('entrenamientos', $entrenamientos);
		}

		else{
	 
			abort(404);                                
	}
		}


		public function update(Request $request, $id){


		$identificacion = Auth::user()->identificacion;
		$entrenamientos = entrenamientos::find($id);




		if ($entrenamientos->identificacion == $identificacion ) {

			$entrenamientos->nombre = $request->nombre;
			$entrenamientos->save();

			 Flash::success("Se ha actualizado el entrenamiento ". $entrenamientos->id. " de forma existosa"); 

              return back();      
		}

		else{
	 
			abort(404);                                
	}
		}


		public function destroy($id){

				$identificacion = Auth::user()->identificacion;
				$entrenamientos= entrenamientos::find($id);
    				
		  		

			  if ($entrenamientos->identificacion == $identificacion ) {
				$entrenamientos= entrenamientos::find($id);
			  		$entrenamientos->delete();
			  		Flash::warning('El entrenamiento '.$entrenamientos->id.' ha sido borrado');
			  		return back();    
				}

			else{
		 
				abort(404);                                
				}	 
    	}





		  
}
