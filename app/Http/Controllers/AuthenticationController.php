<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\empresas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends Controller
{
    public function login(Request $request){



    	$user= User::where('email', $request->email)->first();

       
    	if($user){

    		if(Hash::check($request->password, $user->password)){

    			$token = $user->createToken('Laravel Password Grant Client');

                 $ide= $user->id;

        $u = DB::table('users')
                
                ->join('empresas', 'empresas.identificacion', '=', 'users.identificacion')
                ->select('users.*', 'empresas.razon_social')
                ->where('users.id', '=', $ide)->first();

                


    			$response = ['token' => $token, 'user' => $u];
    			return response($response,200);
    		}
    		else{
    			$response = 'Password mismatch';
    			return response($response, 422);
    		}
    	}
    	else{
    		$response = 'User doesn\'t exist';
    		return response ($response, 422);
    	}
    }

    public function registro(Request $request){

        $user= User::where('email', $request->email)->first();


        if($user){
            $response = 'Lo sentimos! el usuario ya existe';
            return response ($response, 422);

        }

        else{

            $user= new User($request->all());
            $user->password = bcrypt($request->password);
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $user->save();
            

            return response()->json(['success' => $success, 'usuario' => $user]);

        }
    }

    public function viewcentros(Request $request){

        $empresas= empresas::select('identificacion', 'razon_social')->get();

                    return response()->json(['centros' => $empresas]);
    }
}
