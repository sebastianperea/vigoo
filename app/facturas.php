<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facturas extends Model
{
    protected $table="facturas";	
   	protected $fillable= ['id','identificacion','id_plan','fecha_factura','fecha_venc','notas','observaciones','subtotal', 'iva', 'total'];

   	public function empresas()
   {

      return $this->hasMany('App\empresas','identificacion');

   }

   public function planes()
   {

      return $this->belongsToMany('App\planes');

   }
}
