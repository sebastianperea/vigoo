<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class planes extends Model
{
   protected $table="planes";
   protected $fillable= ['id','nombre','precio','detalles'];

   public function empresas()
   {

	return $this->belongsToMany('App\empresas','id');

   } 

    public function facturas()
   {

	return $this->belongsToMany('App\facturas','id')->withPivot('cantidad','precio');

   } 

    public function ordenes()
   {

  return $this->belongsToMany('App\ordenes','id');

   } 
}
